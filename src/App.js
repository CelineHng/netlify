import './App.css';

const name = [
{nom: "Ingrida Lavrinovica", photo: "/ingrida.jpg"},
{nom: "Viktors", photo: "/viktors.jpg"},
{nom: "Mohamed Amine Abdessemed", photo :"/Mohamed.png"},
{nom: "Mélissa Giacomini", photo: "/Melissa.png"},
{nom: "Mark Guoi", photo: "/mark.png"},
]

function App() {
  return (
    <div className="App">
    { name.map( n =>
    <div> 
      <h3> {n.nom}</h3>
      <img src={n.photo} alt=""/>
    </div>
    )}
    </div>
  );
}

export default App;
